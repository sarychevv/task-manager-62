package ru.t1.sarychevv.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.sarychevv.tm.api.endpoint.IUserEndpoint;
import ru.t1.sarychevv.tm.api.service.IAuthService;
import ru.t1.sarychevv.tm.api.service.dto.IUserDTOService;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.dto.request.user.*;
import ru.t1.sarychevv.tm.dto.response.user.*;
import ru.t1.sarychevv.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Autowired
    private IUserDTOService userDTOService;

    @Autowired
    private IAuthService authService;

    @Override
    @NotNull
    @WebMethod
    public UserLockResponse lockUser(@WebParam(name = REQUEST, partName = REQUEST)
                                     @NotNull final UserLockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull UserDTO user = userDTOService.lockUserByLogin(login);
        return new UserLockResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserUnlockResponse unlockUser(@WebParam(name = REQUEST, partName = REQUEST)
                                         @NotNull final UserUnlockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull UserDTO user = userDTOService.unlockUserByLogin(login);
        return new UserUnlockResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserRemoveResponse removeUser(@WebParam(name = REQUEST, partName = REQUEST)
                                         @NotNull final UserRemoveRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable UserDTO user = userDTOService.removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull final UserUpdateProfileRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final String lastName = request.getLastName();
        @NotNull final String firstName = request.getFirstName();
        @NotNull final String middleName = request.getMiddleName();
        @Nullable UserDTO user = userDTOService.updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(@WebParam(name = REQUEST, partName = REQUEST)
                                                         @NotNull final UserChangePasswordRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable UserDTO user = userDTOService.setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserRegistryResponse registryUser(@WebParam(name = REQUEST, partName = REQUEST)
                                             @NotNull final UserRegistryRequest request) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @Nullable UserDTO user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }

}
