package ru.t1.sarychevv.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

    @Transactional
    void removeOne(@Nullable SessionDTO session);

}
