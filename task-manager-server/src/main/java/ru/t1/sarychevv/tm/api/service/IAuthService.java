package ru.t1.sarychevv.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    UserDTO registry(@NotNull String login, @NotNull String password, @NotNull String email);

    String login(@Nullable String login, @Nullable String password);

    @SneakyThrows
    SessionDTO validateToken(@Nullable String token);

    void invalidate(SessionDTO session);

}
