package ru.t1.sarychevv.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.enumerated.Role;

public interface IUserDTOService extends IDTOService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable final String login,
                   @Nullable final String password);

    @NotNull
    UserDTO create(@Nullable final String login,
                   @Nullable final String password,
                   @Nullable final String email);

    @NotNull
    UserDTO create(@Nullable final String login,
                   @Nullable final String password,
                   @Nullable final Role role);

    @Nullable
    UserDTO findOneById(@Nullable final String userId);

    @Nullable
    UserDTO findByLogin(@Nullable final String login);

    @Nullable
    UserDTO findByEmail(@Nullable final String email);

    Boolean isLoginExists(@Nullable final String login);

    Boolean isEmailExists(@Nullable final String email);

    @NotNull UserDTO lockUserByLogin(@Nullable final String login);

    @Nullable UserDTO removeByLogin(@Nullable final String login);

    @Nullable UserDTO setPassword(@Nullable final String id,
                                  @Nullable final String password);

    @NotNull UserDTO unlockUserByLogin(@Nullable final String login);

    @Nullable UserDTO updateUser(@Nullable final String id,
                                 @Nullable final String firstName,
                                 @Nullable final String lastName,
                                 @Nullable final String middleName);

}

