package ru.t1.sarychevv.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> {

    boolean existsById(@Nullable String userId,
                       @Nullable String id);

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId,
                    @Nullable Sort sort);

    @Nullable
    M findOneById(@Nullable String userId,
                  @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId,
                     @Nullable Integer index);

    Integer getSize(@Nullable String userId);

    @Transactional
    void removeOne(@Nullable String userId,
                   @Nullable M model);

    void removeOneById(@Nullable String userId,
                       @Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable String userId,
                       @Nullable Integer id);

    @NotNull
    @Transactional
    M add(@Nullable String userId,
          @Nullable M model);

    void removeAll(@Nullable String userId);

    @Transactional
    void update(@Nullable String userId,
                @Nullable M model);

    @NotNull
    M changeStatusById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable Status status);

    @NotNull
    M changeStatusByIndex(@Nullable String userId,
                          @Nullable Integer index,
                          @Nullable Status status);

    @NotNull
    M updateById(@Nullable String userId,
                 @Nullable String id,
                 @Nullable String name,
                 @Nullable String description);

    @NotNull
    M updateByIndex(@Nullable String userId,
                    @Nullable Integer index,
                    @Nullable String name,
                    @Nullable String description);

}

