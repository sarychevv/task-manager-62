package ru.t1.sarychevv.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.model.ITaskRepository;
import ru.t1.sarychevv.tm.api.service.model.IProjectService;
import ru.t1.sarychevv.tm.api.service.model.ITaskService;
import ru.t1.sarychevv.tm.api.service.model.IUserService;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.exception.user.UserNotFoundException;
import ru.t1.sarychevv.tm.model.Task;
import ru.t1.sarychevv.tm.model.User;

import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    protected ITaskRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId,
                              @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return repository.findAllByUserId(userId);
        @Nullable User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        @Nullable List<Task> tasks = repository.findByUser(user, sort);
        return tasks;
    }

    @NotNull
    @Override
    @Transactional
    public Task changeStatusById(@Nullable final String userId,
                                 @Nullable final String id,
                                 @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String userId,
                       @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setName(name);
        task.setUser(userService.findOneById(userId));
        return repository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String userId,
                       @Nullable final String name,
                       @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(userService.findOneById(userId));
        return repository.save(task);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId,
                                         @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public Task updateById(@Nullable final String userId,
                           @Nullable final String id,
                           @Nullable final String name,
                           @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task updateByIndex(@Nullable final String userId,
                              @Nullable final Integer index,
                              @Nullable final String name,
                              @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Override
    @Transactional
    public void updateProjectIdById(@Nullable final String userId,
                                    @Nullable final String id,
                                    @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (projectId == null) throw new ProjectIdEmptyException();
        task.setProject(projectService.findOneById(userId, projectId));
        repository.save(task);
    }

}

