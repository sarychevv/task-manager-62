package ru.t1.sarychevv.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId,
                   @Nullable String name);

    @NotNull
    Project create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description);

}

