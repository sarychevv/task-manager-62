package ru.t1.sarychevv.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.model.AbstractUserOwnedModel;
import ru.t1.sarychevv.tm.model.User;

import java.util.List;

@NoRepositoryBean
public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @Nullable
    M findByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAllByUserId(@Nullable String userId);

    int countByUserId(@Nullable String userId);

    @NotNull
    List<M> findByUser(@NotNull final User user, @NotNull final Sort sort);

}

