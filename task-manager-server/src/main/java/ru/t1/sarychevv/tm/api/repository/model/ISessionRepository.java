package ru.t1.sarychevv.tm.api.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.model.Session;

@Repository
public interface ISessionRepository extends IUserOwnedRepository<Session> {
}