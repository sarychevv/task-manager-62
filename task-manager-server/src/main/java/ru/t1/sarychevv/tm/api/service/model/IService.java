package ru.t1.sarychevv.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> {

    @Transactional
    M add(M model);

    @Transactional
    void removeAll();

    boolean existsById(@Nullable String id);

    @Nullable List<M> findAll();

    @Nullable
    List<M> findAll(@Nullable Sort sort);

    @Nullable
    M findOneById(@Nullable String id);

    @NotNull
    Integer getSize();

    @Transactional
    void removeOne(M model);

    void removeOneById(@Nullable String id);

    @NotNull
    @Transactional
    M update(M model);
}

