package ru.t1.sarychevv.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.dto.model.AbstractModelDTO;
import ru.t1.sarychevv.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

public interface IDTOService<M extends AbstractModelDTO> {

    @Transactional
    M add(M model);

    @Transactional
    void removeAll();

    boolean existsById(@Nullable String id);

    @Nullable List<M> findAll();

    @Nullable List<M> findAll(@Nullable Comparator comparator);

    @Nullable
    List<M> findAll(@Nullable Sort sort);

    @Nullable
    M findOneById(@Nullable String id);

    Integer getSize();

    @Transactional
    void removeOne(@Nullable M model);

    void removeOneById(@Nullable String id);

    @NotNull
    @Transactional
    M update(@Nullable M model);
}

