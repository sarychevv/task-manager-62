package ru.t1.sarychevv.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.sarychevv.tm.dto.model.AbstractModelDTO;
import ru.t1.sarychevv.tm.enumerated.Sort;

import java.util.List;

@NoRepositoryBean
public interface IDTORepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

    @NotNull
    List<M> findAll(@NotNull final Sort sort);

}
