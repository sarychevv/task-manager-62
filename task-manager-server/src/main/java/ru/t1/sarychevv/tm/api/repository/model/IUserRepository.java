package ru.t1.sarychevv.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.model.User;

@Repository
public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

}

