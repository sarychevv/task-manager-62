package ru.t1.sarychevv.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.sarychevv.tm.api.service.dto.ITaskDTOService;
import ru.t1.sarychevv.tm.api.service.dto.IUserDTOService;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.exception.user.UserNotFoundException;

import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
public final class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO, ITaskDTORepository> implements ITaskDTOService {

    @NotNull
    @Autowired
    private ITaskDTORepository repository;

    @NotNull
    @Autowired
    private IUserDTOService userDTOService;

    @NotNull
    protected ITaskDTORepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId,
                                 @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return repository.findAllByUserId(userId);
        @Nullable UserDTO user = userDTOService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        @Nullable List<TaskDTO> tasks = repository.findByUser(user, sort);
        return tasks;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeStatusById(@Nullable final String userId,
                                    @Nullable final String id,
                                    @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String userId,
                          @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        return repository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return repository.save(task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId,
                                            @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final String name,
                              @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateByIndex(@Nullable final String userId,
                                 @Nullable final Integer index,
                                 @Nullable final String name,
                                 @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Override
    @Transactional
    public void updateProjectIdById(@Nullable final String userId,
                                    @Nullable final String id,
                                    @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        repository.save(task);
    }

}

