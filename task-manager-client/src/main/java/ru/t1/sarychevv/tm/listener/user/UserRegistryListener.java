package ru.t1.sarychevv.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.dto.request.user.UserRegistryRequest;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class UserRegistryListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Registry user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    @EventListener(condition = "@UserRegistryListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setEmail(email);
        request.setLogin(login);
        request.setPassword(password);
        @Nullable final UserDTO user = getUserEndpoint().registryUser(request).getUser();
        showUser(user);
    }

}
