package ru.t1.sarychevv.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class TaskStartByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Start task by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    @EventListener(condition = "@TaskStartByIdListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setStatus(Status.IN_PROGRESS);
        request.setId(id);
        getTaskEndpoint().changeTaskStatusById(request);
    }

}
