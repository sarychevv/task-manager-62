package ru.t1.sarychevv.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class TaskCompleteByIndexListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-index";
    }

    @Override
    @EventListener(condition = "@TaskCompleteByIndexListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(getToken());
        request.setStatus(Status.COMPLETED);
        request.setIndex(index);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

}
