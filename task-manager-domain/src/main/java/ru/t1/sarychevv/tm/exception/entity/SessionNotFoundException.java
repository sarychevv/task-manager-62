package ru.t1.sarychevv.tm.exception.entity;

public final class SessionNotFoundException extends AbstractEntityNotFoundException {

    public SessionNotFoundException() {
        super("Error! Session not found...");
    }

}
