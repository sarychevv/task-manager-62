package ru.t1.sarychevv.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.model.IWBS;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractWorkModel implements IWBS {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    public Task(@NotNull final String name) {
        this.name = name;
    }

    public Task(@NotNull final String name,
                @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
