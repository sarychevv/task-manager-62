package ru.t1.sarychevv.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
